<?php include 'header.php'; ?>
    <div id="container">
  
    <?php 
        include 'data.php';
        $list_film = getAllFilm();
    ?>
    <?php foreach($list_film as $film){ 
    ?>

        <div id="film<?php echo $film['id']; ?>" class="card">
                <img src="<?php echo $film['imgURL']; ?>" alt="keanu">
            <h2>
                <?php echo $film["titre"]; ?>
            </h2>
            <p>
                <?php echo $film["personnage"]; ?>
            </p>
            <?php
                $list_categories = getCatFromFilmID($film['id']);
            ?>
            Catégories :
            <ul>
                <?php for($i = 0; $i<count($list_categories); $i++){ ?>
                    <li><?= $list_categories[$i]["nom"]; ?></li>
                <?php } ?>
            </ul>
        </div>

    <?php } ?>
    </div>
<?php include 'footer.php'; ?>
