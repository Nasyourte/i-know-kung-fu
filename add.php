<?php include 'header.php'; ?>

<form action="insert.php" method="post">

    <label for="titre">Titre du film</label>
    <input id="titre" type="text" name="titre" required>

    <label for="imgURL">Url de l'image</label>
    <input id="imgURL" type="text" name="imgURL" required>

    <label for="perso">Nom du personage</label>
    <input id="perso" type="text" name="personnage" required>

    <label for="john">John</label>
    <input type="checkbox" name="john" id="john">

    <label for="Drama">Drama</label>
    <input type="checkbox" name="Drama" id="Drama">
    
    <input type="submit" value="Ajouter">
</form>

<?php include 'footer.php'; ?>