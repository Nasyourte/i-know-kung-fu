/* dans la console 
sudo mysql -t < init.sql
 */
DROP DATABASE IF EXISTS keanu;
CREATE DATABASE keanu;

USE keanu;

CREATE TABLE film (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    titre VARCHAR(30) not null,
    personnage VARCHAR(30),
    imgURL VARCHAR(255)
);

CREATE TABLE cat (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(30) not null
);

CREATE TABLE film_cat (
    id_film INT UNSIGNED,
    id_cat INT UNSIGNED,
    UNIQUE(id_film, id_cat),
    FOREIGN KEY (id_film) REFERENCES film(id),
    FOREIGN KEY (id_cat) REFERENCES cat(id)
);

GRANT ALL PRIVILEGES ON keanu.* TO 'bob'@'localhost';

INSERT INTO film (titre, personnage, imgURL)
VALUES (
    "Matrix",
    "Neo",
    "https://sm.ign.com/t/ign_fr/news/h/hideo-koji/hideo-kojima-seems-to-be-teasing-the-new-matrix-movie_tjt9.h720.jpg"
);

SELECT * FROM film;