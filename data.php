<?php
include 'debug.php';

// connect à la db
$host = 'localhost';
$db   = 'keanu';
$user = 'bob';
$pass = 'toto'; // ne pas faire ça dans la vrai vie !

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);


// select tous les films
function getAllFilm(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM film');
    return $req->fetchAll();
}

function insertFilm($titre, $perso, $imgURL){
    global $pdo;
    $req = $pdo->prepare('
        INSERT INTO film (titre, personnage, imgURL)
        VALUES (?, ?, ? );');
    $req->execute([$titre, $perso, $imgURL]);
    return $pdo->lastInsertId();
}

function creerCat($nom){
    global $pdo;
    $req = $pdo->prepare('insert into cat(nom) values(?);');
    $req->execute([$nom]);
}

function lierCatFilm($id_film, $id_cat){
    global $pdo;
    $req = $pdo->prepare('insert into film_cat(id_film, id_cat) values (?,?);');
    $req->execute([$id_film, $id_cat]);
}

function insertFilmCat($titre, $perso, $imgURL, $cats){
    $id_film = insertFilm($titre, $perso, $imgURL);
    foreach($cats as $id_cat){
        lierCatFilm($id_film, $id_cat);
    }
}

function getCatFromFilmID($id){
    //recupere la liste des noms des catégories d'un film en fonction de son id
    global $pdo;
    $req = $pdo->prepare('SELECT cat.nom FROM cat, film_cat WHERE cat.id = film_cat.id_cat AND film_cat.id_film = ?;');
    $req->execute([$id]);
    return $req->fetchAll();
}


//